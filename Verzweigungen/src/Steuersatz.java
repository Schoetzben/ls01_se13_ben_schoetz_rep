
import java.util.Scanner;

public class Steuersatz {

	static int nettobetrag;
	static double bruttobetrag;
	static double Steuern; 
	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		// Aufgabe 2
		System.out.println("Aufgabe 2: 'Steuersatz' ");
		System.out.println();
		Steuern = eingabeNetto();
		bruttobetrag = eingabeJaNein();
		
	}
	
	public static int eingabeNetto() {
		
		nettobetrag = tastatur.nextInt();
		if (nettobetrag <= 9.408) {
			return (14);
		} else if (nettobetrag <= 57.052) {
			return (42);
		} else if (nettobetrag >= 270.501) {
			return (45);
		} else {
			return (0);
		}
	}
	
	public static double eingabeJaNein() {
		String jaNein;
		jaNein = tastatur.toString();
		if (jaNein == "j") {
			return (nettobetrag);
		} else if (jaNein == "n") {
			return (nettobetrag + (nettobetrag / 100) * Steuern);
		}
		return (nettobetrag);
		
	}

}
