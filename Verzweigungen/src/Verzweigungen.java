
public class Verzweigungen {

	public static void main(String[] args) {
		System.out.println("Aufgabe 1.1-1.4");
		Aufgabe1();
		System.out.println();
		System.out.println("Aufgabe 1.5-1.7");
		Aufgabe1_2();
		
	}
	
	
	 public static void Aufgabe1() {
		 
	 //Aufgabe 1.1 
		 //1.) a. Wenn die Ampel Rot ist DANN bleib stehen; Wenn der Computer an ist, DANN fang an zu arbeiten
		
	 //Aufgabe 1.2
			
		int a =2, b = 5;
			
		if (a == b) {														//�berpr�fung ob a genau so gro� wie b
			System.out.println("A ist genau so gro� wie B.");
		}
		
	 //Aufgabe 1.3
		
		if (a < b) {														//�berpr�fung ob b gr��er als a ist
			System.out.println("B ist gr��er als A.");
		}
	
	//Aufgabe 1.4 
		
		if (a >= b) {														//�berpr�fung ob a genauso gro� oder gr��er als b ist	
			System.out.println("A ist genau so gro� oder gr��er als B.");
		}
			else {															//Else ausgabe wenn a nicht gr��er und nicht gleich gro� ist
				System.out.println("A ist nicht gr��er als B und auch nicht gleich gro�.");
			}

	}
	 
	 public static void Aufgabe1_2() {
		 
		 int a = 3, b = 4, c = 5;
		 
		 //Aufgabe 1.5
		 	if (a > b && a > c) {											//�berpr�fung ob A gr��er als B und C ist
		 		System.out.println("A ist gr��er als B und C.");
		 	}
		 	
		 //Aufgabe 1.6
		 	if (c > b || c > a) {											//�berpr�fung ob C gr��er als B und A ist
		 		System.out.println("C ist gr��er als B und A.");
		 	}
		 	
		 //Aufgabe 1.7														//�berpr�fung welche die gr��te Zahlt ist
		 	if (a > b && a > b) {											//�berpr�fung ob A die gr��te Zahl ist
		 		System.out.println("A ist die gr��te der 3 Zahlen.");
		 	}
		 	else if (b > a && b > c) {										//�berpr�fung ob B die gr��te Zahl ist
		 		System.out.println("B ist die gr��te der 3 Zahlen.");
		 	}
		 	else {															//�berpr�fung ob C die gr��te Zahl ist
		 		System.out.println("C ist die gr��te der 3 Zahlen.");
		 	}			

			  
	 }

}
