import java.util.Scanner; // Import der Klasse Scanner(Eingaben über die Tastaur)

public class RechnerErklaerung
{

 public static void main(String[] args) // Hier startet das Programm
 {

 // Neues Scanner-Objekt myScanner wird erstellt
 Scanner myScanner = new Scanner(System.in);

  //Ausgabeaufforderung zur Eingabe des ersten Summanten
 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

 // Die Variable zahl1 speichert die erste Eingabe
 int zahl1 = myScanner.nextInt();
 
 //Ausgabeaufforderung zur Eingabe des zweiten Summanten
 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

 // Die Variable zahl2 speichert die zweite Eingabe
 int zahl2 = myScanner.nextInt();

 // Die Addition der Variablen zahl1 und zahl2
 // wird der Variable ergebnis zugewiesen.
 int ergebnis = zahl1 + zahl2;

 
 //Ausgabe des Ergebnisses
 System.out.print("\n\n\nErgebnis der Addition lautet: ");
 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
 myScanner.close(); //Beenden der Eingabemöglichkeit

 }
}