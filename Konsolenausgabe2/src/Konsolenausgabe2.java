
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		
		//Aufgabe 2.1
		
		System.out.println("Aufgabe 2.1:");
		
		System.out.printf("%20s%n", "**"); 
		System.out.printf( "%16s", "*" );
		System.out.printf( "%7s\n", "*", "\n");
		System.out.printf( "%16s", "*" );
		System.out.printf( "%7s\n", "*");
		System.out.printf("%20s%n", "**"); 
		
		System.out.print("\n\n\n");
		
		//Aufgabe 2.2:
		
		System.out.println("Aufgabe 2.2:");
		
		int n = 0;
		
		System.out.printf("%n%-5s", "!" + n); 
		System.out.printf("="); 
		System.out.printf("%-19s", ""); 
		System.out.printf("="); 
		System.out.printf("%4s", "1", "\n"); 
		
		n = 1;
		
		System.out.printf("%n%-5s", "!" + n); 
		System.out.printf("="); 
		System.out.printf("%-19s", " 1"); 
		System.out.printf("="); 
		System.out.printf("%4s", "1", "\n"); 
		
		n = 2;
		
		System.out.printf("%n%-5s", "!" + n); 
		System.out.printf("="); 
		System.out.printf("%-19s", " 1 * 2"); 
		System.out.printf("="); 
		System.out.printf("%4s", "2", "\n");
		
		n = 3;
		
		System.out.printf("%n%-5s", "!" + n); 
		System.out.printf("="); 
		System.out.printf("%-19s", " 1 * 2 * 3"); 
		System.out.printf("="); 
		System.out.printf("%4s", "6", "\n");
		
		n = 4;
		
		System.out.printf("%n%-5s", "!" + n); 
		System.out.printf("="); 
		System.out.printf("%-19s", " 1 * 2 * 3 * 4"); 
		System.out.printf("="); 
		System.out.printf("%4s", "24", "\n");
		
		n = 5;
		
		System.out.printf("%n%-5s", "!" + n); 
		System.out.printf("="); 
		System.out.printf("%-19s", " 1 * 2 * 3 * 4 * 5"); 
		System.out.printf("="); 
		System.out.printf("%4s", "120", "\n");
		
		System.out.print("\n\n\n");
		
		
		
		//Aufgabe 2.3:
		
		System.out.println("Aufgabe 2.3:");
		System.out.println("\n");
		
		System.out.printf("%10s" , "Fahrenheit");
		System.out.printf("%4s", "|");							//Tabellenkopf
		System.out.printf("%11s", "Celsius\n");
		System.out.println("------------------------");	
		
		System.out.printf("%-1s" , "-20");						//Augabe von Fahrenheitwert
		System.out.printf("%11s", "|");
		double f1 = -28.8889;									//deklarierung und INtialisierung des Celsius wertes
		System.out.printf("%10.2f\n", f1);						//Augabe von Celsiuswert
		
		System.out.printf("%-1s" , "-10");						//Augabe von Fahrenheitwert
		System.out.printf("%11s", "|");
		double f2 = -23.3333;									//deklarierung und INtialisierung des Celsius wertes
		System.out.printf("%10.2f\n", f2);						//Augabe von Celsiuswert
		
		System.out.printf("%-1s" , "0");						//Augabe von Fahrenheitwert
		System.out.printf("%13s", "|");
		double f3 = -17.7778;									//deklarierung und INtialisierung des Celsius wertes
		System.out.printf("%10.2f\n", f3);						//Augabe von Celsiuswert
		
		System.out.printf("%-1s" , "20");						//Augabe von Fahrenheitwert
		System.out.printf("%12s", "|");
		double f4 = -6.6667;									//deklarierung und INtialisierung des Celsius wertes
		System.out.printf("%10.2f\n", f4);						//Augabe von Celsiuswert
		
		System.out.printf("%-1s" , "30");						//Augabe von Fahrenheitwert
		System.out.printf("%12s", "|");
		double f5 = -1.1111;									//deklarierung und INtialisierung des Celsius wertes
		System.out.printf("%10.2f\n", f5);						//Augabe von Celsiuswert
		
		
	}

}
