﻿import java.util.Scanner;
	/*	Autor: 		Ben Schötz
	 *  Klasse :	SE13
	 *  Datum :		18.05.2022
	 */
class Fahrkartenautomat
{ //Statische Variablen Deklaration für feste Variablen, die man ohne übergabe in allen Methoden gemeinsam nutzt
	
	static int anzahlTicket;															//Deklarierung globaler Variablen, abspeicherung der Daten unabhängig von Methoden
	static double zuZahlenderBetrag; 													//Deklaration der Var "zuZahlenderBetrag"
	static float eingezahlterGesamtbetrag;												//Deklaration der Var "eingezahlter Betrag"
	static Scanner tastatur = new Scanner(System.in);									//Deklarierung des Scanners "Tastatur"
	static int millisekunde = 250 ;														//REaktionszeicht für die Visualisierung der Berechnung nach der Eingabe
	static double rückgabebetrag;														//float statt double verwendet, da bei double Rundungsfehler entstehen
	static int automatStop; 															//Automat auf An setzen, damit er beim auführen funktioniert
	static int v =0;																	//Deklaration Zählvariable
	static int automatwiederholungen = 1;												//Deklaration von Variable um den Automaten wiederholt laufen zu lassen
	static String [] FahrkartenName = {"", "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};		//Fahrkartenname Array
	static double [] FahrkartenKosten = { 0, 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};		//Fahrkartenkosten Array
	static double preis1 = FahrkartenKosten[1];     								    //Deklaration und Initialiesierung des Fahrkartenpreises
	static double preis2 = FahrkartenKosten[2];											//Array ausgaben müssen erst zu Variable konvertiert werden um das Rechnen zu ermöglichen
	static double preis3 = FahrkartenKosten[8];  
	
	//KassenAdministration
	static boolean kassegefuellt = true;												//Kassenüberprüfung für die Methoden
	static boolean stoerung = false;
	static double eingeworfeneMünze = 0;	
	
	//Kasseninhalt
	static double fuenfzigEuroSchein = 3;
	static double zwanigeuroSchein = 6;
	static double zehnEuroSchein = 5;
	static double fuenfEuroSchein = 10;
	static double zweiEuroMuenze = 25;
	static double einEuroMuenze = 50;
	static double fuenfzigCentMuenze = 50;
	static double zwanzigCentMuenze = 30;
	static double zehnCentMuenze = 10;
	static double fuenfCentMuenze = 15;
	
    public static void main(String[] args) {											//Main Methode              
      
       for(v=0; v < automatwiederholungen;) {											//Schleifenanfang (Endlosschleife)
    	   if(kassegefuellt == true) {
    		   zuZahlenderBetrag = fahrkartenbestellungErfassen(); 						//Aufruf Methode fahrkartenbestellungErfassen
    		   fahrkartenBezahlen();													//Aufruf Methode fahrkartenBezahlen
    		   	if (stoerung == false) {
    		   		fahrkartenAusgeben();													//Aufruf Methode fahrkartenAusgeben
    		   		rueckgeldAusgeben();								//Aufruf Methode rueckgeldAusgeben							
    		   		System.out.println();
    		   		System.out.println();
    		   		automatwiederholungen = automatstoppen(automatwiederholungen);			//Abfrage Methode, ob weitere Zahlungen gewünscht sind
    		   		v++;
    	   		}	
    	   }
       }
       tastatur.close();
    }
    
    public static double fahrkartenbestellungErfassen() {								//Methode Bestellung Erfassen
    	int wunschtiket, kb = 0, weitereKarte = 1;
    	boolean richtigeEingabe;	
    	
    	 System.out.println("Guten Tag werter Fahrer, ");
    	 System.out.println("Sie befinden sich im Auswahlmenü des Ticketautomaten am Hauptbahnhof Köln.");
    	 System.out.println();
    	 System.out.print("Bitte wählen Sie nun Ihre Wunschkarte aus: ");				//Ticketpreis
    	 for  (kb = 0; kb < weitereKarte;) {											//Zahlschleife für die Eingabe
    		richtigeEingabe = false;
    		System.out.println();
    		System.out.println(FahrkartenName[1] + " [" + FahrkartenKosten[1] + " €] (1)");
    		System.out.println(FahrkartenName[2] + " [" + FahrkartenKosten[2] + " €] (2)");
    		System.out.println(FahrkartenName[8] + " [" + FahrkartenKosten[8] + " €] (3)");
    		System.out.println("Bezahlen (9)");
    		System.out.println("Kassenadministration (Code eingeben)");
    		System.out.println();
    		System.out.println("Die Zwischensumme beträgt : " + zuZahlenderBetrag);
    		System.out.println("Bitte tätigen Sie jetzt Ihre Eingabe: ");
    		
    		// Die Neue Implementierung der FahrkartenPriese und Namen ermöglicht den Austausch der Tiket anzeige und der berechnetetn Kosten, durch einfaches Ändern des Indexes
         
         while (richtigeEingabe == false) {
        	 wunschtiket = tastatur.nextInt();    	 
        	 
        	 switch (wunschtiket) {
        	 case 1:																	//Fall eins, wenn der Nutzer den Wert 1 eingibt
        		 zuZahlenderBetrag = zuZahlenderBetrag + preis1;						//Berechnung der Kosten
        		 richtigeEingabe = true;												//Beenden der Schleife, die auf falsche eingaben prüft
        		 weitereKarte++;														//hochzählen der Zählschleife für mehr Fahrkarten
        		 ticketanzahl();														//aufruf der Tickeanzahl Methode
        		 break;																	//Benden des Switch Cases
        	 case 2:																	//Fall zwei, wenn der Nutzer den Wert 2 eingibt
        		 zuZahlenderBetrag = zuZahlenderBetrag + preis2;		
        		 richtigeEingabe = true;
        		 weitereKarte++;
        		 ticketanzahl();
        		 break;
        	 case 3:																	//Fall drei, wenn der Nutzer den Wert 3 eingibt
        		 zuZahlenderBetrag = zuZahlenderBetrag + preis3;		
        		 richtigeEingabe = true;
        		 weitereKarte++;
        		 ticketanzahl();
        		 break;
        	 case 18052022:
        		 kassenadmin();
        	 case 9:																	//Fall vier, wenn der Nutzer den Wert 9 eingibt
        		 richtigeEingabe = true;
        		 break;
        	 default:																	//Fall, wenn der eingegebene Wert mit keinem Fall übereinstimmt
        		 System.out.println(" >>falsche Eingabe<< ");
        		 System.out.println("Bitte tätigen Sie jetzt eine neue Eingabe: ");
        	}
         }
         kb++; 																			//Zahlschleife für die Anzahl der Wiederholungen
      }
    	 return zuZahlenderBetrag;														//Rückgabe des Betrages in die Main methode
    }																					//End of Methode fahrkartenbestellungErfassen
         
    public static void ticketanzahl () {
         //Eingabeaufforderung Anzahl der Tickets
         System.out.println("\nBitte gebeb Sie nun die Anzahl der Tickets ein ");		//Anzahl Tickets eingeben
         System.out.println("beachten Sie, dass Sie nur maximal 10 Tickets ");		
         System.out.print("gleichzeitig erwerben können : ");
         
         //Konsoleneingabe Ticketanzahl
         anzahlTicket = tastatur.nextInt();												//Aufruf Scanner für Tätigung einer Eingabe
         while (anzahlTicket > 10 || anzahlTicket < 0) {								//Beginn Schleife für Ticketanzahl abfrage
        	System.out.println("Da Sie einen Wert eingeben haben der kleiner"
        			+ "als 1 oder größer als 10 ist, ändern wir diesen Wert auf 1."); 
        	anzahlTicket = 1;															//Bei Falscheingabe, wird 1 Ticket festgelegt 
         }       	 
         
         //Multiplikation der Tickets mit dem Ticketpreis zur Errechnung des zu zahlenden Betrages
         zuZahlenderBetrag = zuZahlenderBetrag * anzahlTicket;
    } 																					//end Methode Ticketanzahl
           
    	
    public static void fahrkartenBezahlen() {											//Methode fahrkartenBezahlen

        kassensatus();																	//Aufruf KassenMethode zum Abfragen auf Status abweichung
        
    	// Geldeinwurf
        // -----------
        if (kassegefuellt == true) {
        	eingezahlterGesamtbetrag = 0;
        	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)	{						//Schleife für Geldeingabe, bis die Kosten bezahlt werden        {
        		double betrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
        		System.out.printf("\nBitte zahlen Sie noch: %.2f Euro\n", betrag);
        		System.out.print("Eingeben können Sie (mind. 5Ct, höchstens 50 Euro): ");
        		eingeworfeneMünze = tastatur.nextDouble();								//Scanneraufruf und neue Eingabe
        		eingezahlterGesamtbetrag += eingeworfeneMünze;							//Hinzufügen des neuen betrags zum bestehenden
        		inKasseeinzahlen();														//Eingezahltes Geld der Kasse hinzufügen
        	}
        }else {
        	System.out.println("<< STÖRUNG >>");
        	System.out.println("Aufgrund des Kassenstatus ist es aktuell leider ");
        	System.out.println("nicht möglich Ihnen genug Rückgeld anzubieten.");
        	System.out.println("Die Bestellung wurde abgebrochen und der Admin informiert.");
        	System.out.println();
        	System.out.println("Wir entschuldigen Uns für die Unanehmlichkeiten, ");
        	System.out.println("bitte wenden Sie sich an den Ticketschlater.");
        	stoerung = true;
        }
    }																					//end Methode fahrkartenBezahlen
    
    public static void inKasseeinzahlen() {
    	
    	while(eingeworfeneMünze>0.0) {
    	if(eingeworfeneMünze >= 50.0) {
    		fuenfzigEuroSchein++;
    		eingeworfeneMünze = eingeworfeneMünze - 50;
    	}else if(eingeworfeneMünze >= 40.0) {
    		zwanigeuroSchein = zwanigeuroSchein + 2;
    		eingeworfeneMünze = eingeworfeneMünze - 40.0;
    	}else if(eingeworfeneMünze >= 30.0) {
    		zehnEuroSchein++;
    		zwanigeuroSchein++;
    		eingeworfeneMünze = eingeworfeneMünze - 30.0;
    	}else if(eingeworfeneMünze >= 20.0) {
    		zwanigeuroSchein++;
    		eingeworfeneMünze = eingeworfeneMünze - 20.0;
    	}else if(eingeworfeneMünze >= 15.0) {
    		fuenfEuroSchein++;
    		zehnEuroSchein++;
    		eingeworfeneMünze = eingeworfeneMünze - 15.0;
    	}else if(eingeworfeneMünze >= 10.0) {
    		zehnEuroSchein++;
    		eingeworfeneMünze = eingeworfeneMünze - 10.0;
    	}else if(eingeworfeneMünze >= 5.0) {
    		fuenfEuroSchein++;
    		eingeworfeneMünze = eingeworfeneMünze - 5.0;
    	}else if(eingeworfeneMünze >= 4.0) {
    		zweiEuroMuenze = zweiEuroMuenze + 2;
    		eingeworfeneMünze = eingeworfeneMünze - 4.0;
    	}else if(eingeworfeneMünze >= 2.0) {
    		zweiEuroMuenze++;
    		eingeworfeneMünze = eingeworfeneMünze - 2.0;
    	}else if(eingeworfeneMünze >= 1.0) {
    		einEuroMuenze++;
    		eingeworfeneMünze = eingeworfeneMünze - 1.0;
    	}else if(eingeworfeneMünze >= 0.50) {
    		fuenfzigCentMuenze++;
    		eingeworfeneMünze = eingeworfeneMünze - 0.5;
    	}else if(eingeworfeneMünze >= 0.20) {
    		zwanzigCentMuenze++;
    		eingeworfeneMünze = eingeworfeneMünze - 0.20;
    	}else if(eingeworfeneMünze >= 0.10) {
    		zehnCentMuenze++;
    		eingeworfeneMünze = eingeworfeneMünze - 0.10;
    	}else if(eingeworfeneMünze >= 0.05) {
    		fuenfCentMuenze++;
    		eingeworfeneMünze = eingeworfeneMünze - 0.05;
    	}																				//End of IF
      }																					//End of While schleife
    }																					//End of Methode
        
    
    public static void fahrkartenAusgeben() {											//Methode fahrkartenAusgeben
    	
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 15; i++)													//Visualisierung des BErechnungsprozesses
        {
           System.out.print("=");
           try {
 			warte(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");

    }																					//end Methode fahrkartenAusgeben
    
    public static void warte(int millisekunde) throws InterruptedException {			//Methode warte
    	Thread.sleep(250);																//wartet 250 millisekunden
    	    	
    }																					//end Methode warte
    
    
    public static void rueckgeldAusgeben() {				//Methode rueckgeldAusgeben
    	String einheit = "Euro";
    	// Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;					//Berechnung des Rückgabe Betrags
        if(rückgabebetrag > 0.0)														//Abfrage ob Geld übrig ist
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rückgabebetrag);
     	   System.out.println("für "+ anzahlTicket + " Tickets, wird wie folgend ausgezahlt:");		
     	    muenzeAusgeben(rückgabebetrag, einheit);									//Methodenaufruf und Werte übergabe
            
        } else {																		//Bei keinem Rückgeld wird der Automat beendet
        	System.out.println("Der Rückgabebetrag beträgt: 0.00 Euro.");
        	System.out.println("Sie können nun Ihren Fahrschein entnehmen.");
        }

        System.out.println("\nUnd Vergessen Sie nicht, den/die Fahrschein/e\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt "
                           + "und einen schönen Tag.");
        System.out.println("\nIhr Bahn Team.");
    	
    }																					//end Methode rueckgeldAusgeben
    
    public static void muenzeAusgeben(double betrag, String einheit) {					//Methodenbeginn
    	
    	while(betrag >= 50.0) // 50 Euro Schein
    	{
    	  System.out.println(" ______________________________");
          System.out.println("|                              |");
          System.out.println("|             50               |");
          System.out.println("|         EURO-SCHEIN          |");
          System.out.println("|______________________________|");
    	  	  betrag -= 50.0;
    	  	fuenfzigEuroSchein--;
     	}while(betrag >= 20.0) // 20 Euro Schein
    	{
     	  System.out.println(" ______________________________");
          System.out.println("|                              |");
          System.out.println("|             20               |");
          System.out.println("|         EURO-SCHEIN          |");
          System.out.println("|______________________________|");
    	  	  betrag -= 20.0;
    	  	zwanigeuroSchein--;
     	}while(betrag >= 10.0) // 10 Euro Schein
    	{
     	  System.out.println(" ______________________________");
      	  System.out.println("|                              |");
      	  System.out.println("|             10               |");
      	  System.out.println("|         EURO-SCHEIN          |");
      	  System.out.println("|______________________________|");
    	  	  betrag -= 10.0;
    	  	zehnEuroSchein--;
     	}while(betrag >= 5.0) // 5 Euro Schein
    	{
    	  System.out.println(" ______________________________");
    	  System.out.println("|                              |");
    	  System.out.println("|              5               |");
    	  System.out.println("|         EURO-SCHEIN          |");
    	  System.out.println("|______________________________|");
    	  	  betrag -= 5.0;
    	  	  fuenfEuroSchein--;
     	}
     	while(betrag >= 4.0) 
     	{
     		System.out.println("   * * *   		   * * *   ");
    		System.out.println(" *       * 		 *       * ");
    		System.out.println("*    2    *		*    2    *");
    		System.out.println("*   EURO  *		*   EURO  *");
    		System.out.println(" *       * 		 *       * ");
    		System.out.println("   * * *  		   * * *   ");
	          betrag -= 4.0;
	          zweiEuroMuenze -= 2;
     	}
     	while(betrag >= 3.0) 
     	{
     		System.out.println("   * * *   		   * * *   ");
    		System.out.println(" *       * 		 *       * ");
    		System.out.println("*    2    *		*    1    *");
    		System.out.println("*  EURO   *		*   EURO  *");
    		System.out.println(" *       * 		 *       * ");
    		System.out.println("   * * *  		   * * *   ");
	          betrag -= 3.0;
	          zweiEuroMuenze--;
	          einEuroMuenze--;
     	}
    	while(betrag >= 2.0) // 2 EURO-Münzen
        {
    		System.out.println("   * * *   ");
    		System.out.println(" *       * ");
    		System.out.println("*    2    *");
    		System.out.println("*  EURO   *");
    		System.out.println(" *       * ");
    		System.out.println("   * * *   ");
	          betrag -= 2.0;
	          zweiEuroMuenze--;
        }
        while(betrag >= 1.0) // 1 EURO-Münzen
        {
        	System.out.println("   * * *   ");
    		System.out.println(" *       * ");
    		System.out.println("*    1    *");
    		System.out.println("*  EURO   *");
    		System.out.println(" *       * ");
    		System.out.println("   * * *   ");
	          betrag -= 1.0;
	          einEuroMuenze--;
        }
        while(betrag >= 0.5) // 50 CENT-Münzen
        {
        	System.out.println("   * * *   ");
    		System.out.println(" *       * ");
    		System.out.println("*    50   *");
    		System.out.println("*   CENT  *");
    		System.out.println(" *       * ");
    		System.out.println("   * * *   ");
	          betrag -= 0.5;
	          fuenfzigCentMuenze--;
        }
        while(betrag >= 0.38) {
        	System.out.println("   * * *   		   * * *   ");
    		System.out.println(" *       * 		 *       * ");
    		System.out.println("*    20   *		*    20   *");
    		System.out.println("*   CENT  *		*   CENT  *");
    		System.out.println(" *       * 		 *       * ");
    		System.out.println("   * * *  		   * * *   ");
     	      betrag -= 0.4;
     	     zwanzigCentMuenze -= 2;
        }
        while(betrag >= 0.2) {
        	System.out.println("   * * *   ");
    		System.out.println(" *       * ");
    		System.out.println("*    20   *");
    		System.out.println("*   CENT  *");
    		System.out.println(" *       * ");
    		System.out.println("   * * *   ");
   	          betrag -= 0.2;
   	       zwanzigCentMuenze--;
        }
        while(betrag >= 0.1) // 10 CENT-Münzen
        {
        	System.out.println("   * * *   ");
    		System.out.println(" *       * ");
    		System.out.println("*    10   *");
    		System.out.println("*   CENT  *");
    		System.out.println(" *       * ");
    		System.out.println("   * * *   ");
	          betrag -= 0.1;
	          zehnCentMuenze--;
        }
        while(betrag >= 0.05)// 5 CENT-Münzen
        {
        	System.out.println("   * * *   ");
    		System.out.println(" *       * ");
    		System.out.println("*    5    *");
    		System.out.println("*   CENT  *");
    		System.out.println(" *       * ");
    		System.out.println("   * * *   ");
	          betrag -= 0.05;
	          fuenfCentMuenze--;
        }
    	
    }
    
    public static int automatstoppen(int automatwiederholungen) {
    	
    	System.out.println();
    	System.out.println("Ein weiterer Kauf kann nun getätigt werden.");
    	System.out.println("Wenn Sie fortfahren möchten geben sie bitte: '1' ein");
    	automatStop = tastatur.nextInt();
    	System.out.println();
    	System.out.println();
    	System.out.println();
    	if (automatStop == 1) {
    		automatwiederholungen++;
    		zuZahlenderBetrag = 0.0;
    	}
    	
		return automatwiederholungen;
    }
    
    public static void kassensatus() {													//Methode zum Kasseninhalt
    	
		if(fuenfzigEuroSchein > 1) {
    		kassegefuellt = true;														//Solange Münzen/Scheine da sind muss nicht aufgefüllt werden
    	}else {
    		kassegefuellt = false;														//Wenn keine Scheine/Münzen da => Muss aufgefüllt werden
    	}
		if(zwanigeuroSchein > 1) {
    		kassegefuellt = true;														
    	}else {
    		kassegefuellt = false;														
    	}
		if(zehnEuroSchein > 1) {
    		kassegefuellt = true;														
    	}else {
    		kassegefuellt = false;														
    	}
    	if(fuenfEuroSchein > 1) {
    		kassegefuellt = true;														
    	}else {
    		kassegefuellt = false;														
    	}
    	if(zweiEuroMuenze > 1) {
    		kassegefuellt = true;
    	}else {
    		kassegefuellt = false;
    	}
    	if(fuenfzigCentMuenze > 1) {
    		kassegefuellt = true;
    	}else {
    		kassegefuellt = false;
    	}
    	if(zwanzigCentMuenze > 1) {
    		kassegefuellt = true;
    	}else {
    		kassegefuellt = false;
    	}
    	if(zehnCentMuenze > 1) {
    		kassegefuellt = true;
    	}else {
    		kassegefuellt = false;
    	}
    	if(fuenfCentMuenze > 1) {
    		kassegefuellt = true;
    	}else {
    		kassegefuellt = false;
    	}
       	
    }																					//Methodenende
    
    
    
    public static void kassenadmin() {
    	//locale Variable
    	int admineingabe;
    	int zaehler2 = 1;
    	
    	System.out.println();															//Übersicht für Kasseninfo schaffen
    	System.out.println();																
    	System.out.println();
    	System.out.println();
    	System.out.println();
    	System.out.println();
    	System.out.println();
    	System.out.println("Willkommen werter Administrator!");
       	System.out.println("KassenInformtion <<1>>");
    	System.out.println("Kassen füllen	 <<2>>");
    	System.out.println("Kassen leeren	 <<3>>");
    	System.out.println("Zurück		 <<4>>");
    	
    	for(int zaehler = 0; zaehler < zaehler2; zaehler++) {
    		
    		System.out.println();
    		System.out.println("Was möchten Sie tun? >>Bitte Eingabe<< ");
    		
    		admineingabe = tastatur.nextInt();											//Scanner eingabe Admin
    		switch (admineingabe) {														//swtich Case für Admin eingabe
    			case 1:
    				kassensatus();
    				if(kassegefuellt == true) {
    					System.out.println("Die Kasse ist noch ausreichend gefüllt "
    							+ "und muss nicht unbedingt aufgefüllt werden. ");
    				}else {
    					System.out.println("Die Kasse ist nicht ausreichend gefüllt "
    							+ "und muss sofort aufgefüllt werden. ");
    				}    
    				System.out.println("Fünfzigeuroschein/n :    " + fuenfzigEuroSchein);		//Tatsächlicher Kasseninhalt
    				System.out.println("Zwanigeuroschein/n :    " + zwanigeuroSchein);
    				System.out.println("Zehneuroschein/n :    " + zehnEuroSchein);
    				System.out.println("Fünfeuroschein/e :   " + fuenfEuroSchein);			
    				System.out.println("Zweieuromünze/n :    " + zweiEuroMuenze);
    				System.out.println("Eineuromünze/n :     " + einEuroMuenze);
    				System.out.println("Fünfzigcentmünze/n : " + fuenfzigCentMuenze);
    				System.out.println("Zwanzigcentmünze/n : " + zwanzigCentMuenze);
    				System.out.println("Zehncentmünze/n :    " + zehnCentMuenze);
    				System.out.println("Fuenfcentmünze/n :   " + fuenfCentMuenze);
    				
    				zaehler2++;															//erneute Eingabe ermöglichen
    				break;
    				
    			case 2:																	//Kasse füllen
    				System.out.println("Die Kasse Wurde erfolgreich befüllt.");
    				fuenfzigEuroSchein = 3;
    				zwanigeuroSchein = 6;
    				zehnEuroSchein = 5;
    				fuenfEuroSchein = 10;
    				zweiEuroMuenze = 25;
    				einEuroMuenze = 50;
    				fuenfzigCentMuenze = 50;
    				zwanzigCentMuenze = 30;
    				zehnCentMuenze = 10;
    				fuenfCentMuenze = 15;
    				zaehler2++;															//erneute Eingabe ermöglichen
    				break;
    				
    			case 3:																	//Kasse leeren
    				System.out.println("Die Kasse Wurde erfolgreich geleert.");
    				fuenfzigEuroSchein = 0;
    				zwanigeuroSchein = 0;
    				zehnEuroSchein = 0;
    				fuenfEuroSchein = 0;
    				zweiEuroMuenze = 0;
    				einEuroMuenze = 0;
    				fuenfzigCentMuenze = 0;
    				zwanzigCentMuenze = 0;
    				zehnCentMuenze = 0;
    				fuenfCentMuenze = 0;
    				zaehler2++;															//erneute Eingabe ermöglichen
    				break;
    				
    			case 4:
    				fahrkartenbestellungErfassen();
    		}
    	} 																				//End of switch Case	
    }
    	
}																						//end class