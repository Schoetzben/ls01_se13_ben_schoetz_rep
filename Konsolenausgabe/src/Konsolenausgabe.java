
public class Konsolenausgabe {

	public static void main(String[] args) {
		// Aufgabe 1:

		System.out.print("Das ist ein \"Beispielsatz\". \n ");    	//print() schreibt zwischen den Anweisungen keine Zeilenumbr�che.
		System.out.print("Ein Beispielsatz ist das \n");			//println() f�gt hinter jeder Anweisung einen Zeilenumbruch ein.
		
		int alter = 50;
		String name = "Gustav";
		System.out.println("Ich bin " + name + " und ich bin " + alter + " Jahre alt." );
		
		//Aufgabe 2:
		
		String stern3 = "      ***";;				//Deklarierung von Stern3 und Stern11
		String stern11 = " ***********";
		
		System.out.print(" \n" + "\n");					//einf�gen eines Zeilenabstandes zwischen Aufgaben
		
		System.out.print("       * \n");				//Ausgabe von einem PFeil aus Sternen mit den vorgegebenen M�glichkeiten
		System.out.print(stern3 + " \n");
		System.out.print("     *****\n");
		System.out.print("    *******\n");
		System.out.print("   ********* \n ");
		System.out.print(stern11 + "\n");
		System.out.print(" ************* \n");
		System.out.print(stern3 + "\n");
		System.out.print(stern3 + "\n");
		
		System.out.print("\n" + "\n");		//einf�gen eines Zeilenabstandes zwischen Aufgaben
		
		//Aufgabe 3:
		
		double a = 22.4234234;				//Deklarierung von a-e
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf( " %.2f \n" , a); 			//Ausgabe von a-e mit 2 Stellen nach dem Komma
		System.out.printf( " %.2f \n" , b);
		System.out.printf( " %.2f \n" , c);
		System.out.printf( " %.2f \n" , d);
		System.out.printf( " %.2f \n" , e);
	}

}
