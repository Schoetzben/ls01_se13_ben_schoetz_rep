/*
 * erste Arbeit mit Variablen
 */
 
public class Variablen {						//Klassenname wird gro� geschrieben

	public static void main(String[] args) {	//main Methode, Einsprungspunkt bei der Ausf�hrung
		
		 int meinVermoegen; 					//Deklaration einer ganzzahligen Variablen (Datenyp integer)

		 //Initialisierung der Variablen (Startwert wird zugewiesen)
		 
		 meinVermoegen = 20;

		 //Deklaration und Initialisierung kann auch in einem Schritt erfolgen
		 
		 int paulasVermoegen = 4000;
		 
		 meinVermoegen = meinVermoegen + 100; 							//neuer Wert von �meinVermoegen�: 120
		 
		 System.out.println("meinVermoegen: " + meinVermoegen);
		 
		 System.out.println("paulasVermoegen: " + paulasVermoegen);
		 
		 //��berfall�
		 //Rechte Seite vom = : Berechnungen. Ergebnis wird in Variable auf linker Seite geschrieben
		 
		 meinVermoegen = meinVermoegen + paulasVermoegen; 				//neuer Wert: 120 + 4000

		 //Paulas wurde ausgeraubt, sein Verm�gen wird auf 0 gesetzt
		 
		 paulasVermoegen = 0;

		 System.out.println("meinVermoegen: " + meinVermoegen); 		// 4120
		 System.out.println("paulasVermoegen: " + paulasVermoegen); 		// 0
		
		
	}

}
