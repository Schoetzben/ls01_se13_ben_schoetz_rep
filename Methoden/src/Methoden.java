
public class Methoden {

	public static void main(String[] args) {
	
		int a=5, b=20, erg, erg2, erg3;
		double c=5, d=20, erg4, x = 2.0, y = 4.0, m, z= 5, q;
		
		hilfsmethode();				//Methodenaufruf
		erg = addition(a,b);
		erg2 = subtraktion(a,b);
		erg3 = multilplication(a,b);
		erg4 = division(c,d);
		
		
		System.out.println(a + " + " + b + " = " + erg);
		System.out.println(a + " - " + b + " = " + erg2);
		System.out.println(a + " * " + b + " = " + erg3);
		System.out.println(c + " / " + d + " = " + erg4);
		
		m = mittelwert(x, y);
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
		
		
		
		q = quadrieren(z);
		System.out.printf("x = %.2f und x�= %.2f\n", x, q);
		
	}
	
	
	//Methodendefinition
	 public static void hilfsmethode() {			//Methodenkopf
		 
		 System.out.println("TEST");
	 }
	 
	 public static int addition(int zahl1, int zahl2) {
		 
		 return zahl1 + zahl2;
		 
	 }
	 
	 public static int subtraktion(int zahl1, int zahl2) {
		 
		 return zahl1 - zahl2;
				 
	 }
	 
	 public static int multilplication(int zahl1, int zahl2) {
		 
		 return zahl1 * zahl2;
		 
	 }
	 
	 public static double division(double zahl1, double zahl2) {
		 
		 return zahl1 / zahl2;
		 
	 }
	 
	 public static double mittelwert(double x, double y) { 
	      
	      // Mittelwert von x und y berechnen: 
	      // ================================
	      return (x + y) / 2.0;
	
	 }
	 
	 public static double quadrieren(double z) {
		 
		 return z * z;
		 
	 }
	 
}
