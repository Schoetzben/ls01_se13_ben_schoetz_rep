import java.util.Scanner;

public class ArrayEinfuehrung {

	static int [] myArray = new int[10];
	static Scanner tastatur = new Scanner(System.in);
	static String wiederholung = "Nein";
	
	public static void main(String[] args) {
		
		do {
		wiederholung = "Nein";
		System.out.print("Bitte w�hlen Sie eine Ausf�hrung(1-4) : ");
		int aufgabe = tastatur.nextInt();
		
		switch (aufgabe) { 
			case 1:
				Werteausgeben();
				break;
			case 2:
				BestimmterWert();
				break;
			case 3:
				Neufuellen();
				break;
			case 4:
				Wertaendern(); 
				break;
			default:
				System.out.println("Fehlerhafte Eingabe");
		}
		System.out.print("M�chten Sie eine neue Eingabe t�tigen? Ja/Nein : ");
		wiederholung = tastatur.next();
		}while(wiederholung.equals("Ja"));
		
	}
	
	public static void Werteausgeben() {
		
		System.out.println();
		System.out.println("1.3");
		
		myArray[0] = 2;
		myArray[1] = 3;
		myArray[2] = 0;
		myArray[3] = 8;
		myArray[4] = 6;
		myArray[5] = 23;
		myArray[6] = 7;
		myArray[7] = 21;
		myArray[8] = 4;
		myArray[9] = 10;
		
		for(int i = 0; i <10;i++) {
			
			System.out.println("Box " + i + ": " + myArray[i]);			
		}
	}
	
	public static void BestimmterWert() {

		System.out.println();
		System.out.println("1.5");
		
		System.out.println("Box " + 3 + ": " + myArray[3]);
		
	}
	
	public static void Neufuellen() {

		System.out.println();
		System.out.println("1.6");
		
		for(int i = 0; i <10;i++) {
			myArray[i] = 0;
		}		
		
		for(int i = 0; i <10;i++) {
			
			System.out.println("Box " + i + ": " + myArray[i]);			
		}
	}
	
	public static void Wertaendern() {
		System.out.println();
		System.out.println("1.4");
		
		myArray[3] = 1000;
		System.out.println("Box " + 3 + ": " + myArray[3]);
	}
	}
