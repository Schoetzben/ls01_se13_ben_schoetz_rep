
public class HelloWorld {

	public static void main(String[] args) {
		
		System.out.println("Hello World");
		System.out.println("Guten Tag Ben");
		System.out.println("Willkommen in der Veranstaltung Strukturierte Programmierung.");
		
		//Arbeit mit Variablen
		//Deklaration einer ganzzahligen Variablen "Zahl1"
		//Initialisierung von der Zahl 5
		int zahl1 = 5;
		int zahl2 = 3;
		System.out.println(zahl1 + "" + zahl2);
		System.out.println("zahl1");
	}

}
