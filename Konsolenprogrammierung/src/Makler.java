import java.util.Scanner;

public class Makler {

	public static void main(String[] args) {
		//Aufg. 1
		
		Scanner tastatur = new Scanner(System.in);
		
		double laenge;
		double breite;
		double flaeche;
		
		System.out.println("Geben Sie zuerst die L�nge ein: ");
		laenge = tastatur.nextDouble();
		
		System.out.println("Geben Sie nun die Breite ein: ");
		breite = tastatur.nextDouble();

		flaeche = laenge * breite;
		System.out.println("Die Fl�che des Raumes betr�gt: " + flaeche + " qm");
		
		
		tastatur.close();
	}

}
