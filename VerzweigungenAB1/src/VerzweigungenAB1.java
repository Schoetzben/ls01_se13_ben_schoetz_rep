import java.util.Scanner;

public class VerzweigungenAB1 {

	static Scanner tastatur = new Scanner(System.in);
	static int wert1;
	static int wert2;
	
	public static void main(String[] args) {
		int aufgabe;
	
	System.out.print("Bitte w�hlen Sie eine Aufgabe zw. 1 und 5: ");
	aufgabe = tastatur.nextInt();
	if (aufgabe == 1) {
		aufgabe1();
	} else if (aufgabe == 2) {
		aufgabe2();
	} else if (aufgabe == 3) {
		aufgabe3();
	} else if (aufgabe == 4) {
		aufgabe4();
	} else if (aufgabe == 5) {
		aufgabe5();
	}
	
}
	
	public static void aufgabe1() {
	//Aufgabe 1	
		System.out.println();
		System.out.println("Aufgabe 1");
	System.out.println("Bitte geben Sie jetzt den ersten Wert ein: ");			
	wert1 = tastatur.nextInt();
	System.out.println("Bitte geben Sie jetzt den zweiten Wert ein: ");
	wert2 = tastatur.nextInt();
	System.out.println("Die Werte werden nun mit einander abgeglichen: ");
	if (wert1 == wert2) {
		System.out.println("Gleiche Werte");
	}else System.out.println("Ungleiche Werte");									
	
	}
	
	public static void aufgabe2() {
	//Aufgabe2
		System.out.println();
		System.out.println("Aufgabe 2");
		System.out.println("Bitte geben Sie jetzt den ersten Wert ein: ");	
	wert1 = tastatur.nextInt();
	System.out.println("Bitte geben Sie jetzt den zweiten Wert ein: ");
	wert2 = tastatur.nextInt();
	System.out.println("Die Werte werden nun mit einander abgeglichen: ");
	if (wert1 == wert2) {
		System.out.println(wert1 + " = " + wert2);
	}else if (wert1 > wert2) {
		System.out.println(wert2);
		System.out.println(wert1);
	}else { 
		System.out.println(wert1);
	    System.out.println(wert2);
	}		
		
}
	public static void aufgabe3() {
		//Aufgabe3
			System.out.println();
			System.out.println("Aufgabe 3");
			System.out.println("Bitte geben Sie jetzt den ersten Wert ein: ");	
			wert1 = tastatur.nextInt();
			System.out.println("Der Wert wird nun �berpr�ft: ");
			if (wert1 > 0) {
				System.out.println("Der Wert: " + wert1 + " ist eindeutig positiv ");
			}else System.out.println("Der Wert: " + wert1 + " ist eindeutig negativ ");
			
		
		}
		
	public static void aufgabe4() {
			
	//Aufgabe4
		System.out.println();
		System.out.println("Aufgabe 4");
		System.out.println("Bitte geben Sie jetzt einen Wert zwischen 0 und 100 ein: ");	
		wert1 = tastatur.nextInt();
		System.out.println("Der Wert wird nun �berpr�ft: ");
		if (wert1 >= 0) {
			if (wert1 <= 100) {
				if (wert1 < 50) {
				System.out.println("klein"); }
				if (wert1 > 50) {
				System.out.println("gro�"); }
			}else System.out.println("Fehlerhafte Eingabe");
		}else System.out.println("Fehlerhafte Eingabe");
	}
	
	public static void aufgabe5() {
		System.out.println();
		System.out.println("Aufgabe 5");
		System.out.println("Bitte geben Sie jetzt einen ganzahligen Wert ein: ");	
		wert1 = tastatur.nextInt();
		if (wert1 % 2 == 0) {
			System.out.println("Der Wert: " + wert1 + " : 2 " + " = " + (wert1/2));
		}else System.out.println("Der Wert: " + wert1 + " ist ungerade");
		
	}
}
