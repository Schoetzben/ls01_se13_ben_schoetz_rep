import java.util.Scanner;

public class AB_Schleifen_2 {

	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int zahl = 0;

		
		
		
//####################################################		
//		Aufgabe 1
//####################################################
System.out.println("Aufgabe 1");	
		
		System.out.println("Geben Sie eine zweite Zahl ein");
		zahl = tastatur.nextInt();
		
		//a)
		int i = 1;
		while(i <= zahl) {
			System.out.print(i + " ");
			i++;
		}
		System.out.println("");
		
		//b)
		while(zahl >= 1) {
			System.out.print(zahl + " ");
			zahl--;
		}
		System.out.println("");
		
//####################################################		
//		Aufgabe 2
//####################################################		
System.out.println("Aufgabe 2");
		
		boolean eingabeKorrekt = false;
		int zaehler = 1;
		long ausgabe = 1;
		
		while(eingabeKorrekt == false) {
			System.out.println("Geben Sie eine Zahl ein: (0-20)");
			zahl = tastatur.nextInt();
			if(zahl <= 20)  {eingabeKorrekt = true;}
			else {System.out.println("false Eingabe, bitte erneut eingeben");}
		}
		
		do {
			ausgabe = ausgabe * zaehler;
			zaehler++;
		}while (zaehler <= zahl);
		
		System.out.println(ausgabe);
	}

}
