/**
  *
  * Beschreibung
  *
  * @version 1.1 vom 13.12.2021
  * @author Ben Sch�tz
  */

public class LogischeOperatoren {
  
  public static void main(String[] args) {
    /* 1. Deklarieren Sie zwei Warheitswerte a und b.*/
    
	  boolean a, b;
	  
    /* 2. Initialisieren Sie einen Wert mit true, den anderen mit false */
	   
	  a = true;
	  b = false;
	  
    /* 3. Geben Sie beide Werte aus */
	  
	  System.out.println("A = " + a + ".");
	  System.out.println("B = " + b + ".");
    
    /* 4. Deklarieren Sie einen Wahrheitswert "undGatter" */
	  
	  boolean undGatter;
    
    /* 5. Weisen Sie der Variable "undGatter" den Wert "a AND b" zu 
          und geben Sie das Ergebnis aus. */
	  undGatter = (a && b);
	  System.out.println("Der Wert 'undGatter' mit a ist: " + undGatter);
	  
    /* 6. Deklarieren Sie au�erdem den Wahrheitswert c und initialisieren ihn
          direkt mit dem Wert true */
      
      boolean c = true;
          
    /* 7. Verkn�pfen Sie alle drei Wahrheitswerte a, b und c und geben Sie
          jeweils das Ergebnis aus */
       // a)  a AND b AND c
      System.out.println("Verkn�pfung von a, b und c ist : " + (a && b && c));
      
       // b)  a OR  b OR  c
      System.out.println("Verkn�pfung von a oder b oder c ist : " + (a || b || c));
      
       // c)  a AND b OR  c
      System.out.println("Verkn�pfung von a und b oder c ist : " + (a && b || c));
      
       // d)  a OR  b AND c
      System.out.println("Verkn�pfung von a oder b und c ist : " + (a || b && c));
      
       // e)  a XOR b AND c
      System.out.println("Verkn�pfung von a xoder b und c ist : " + (a ^ b && c));
      
       // f) (a XOR b) OR c
      System.out.println("Verkn�pfung von (a xoder b) oder c ist : " + ((a ^ b) && c));
      
       
  } // end of main

} // end of class LogischeOperatoren
