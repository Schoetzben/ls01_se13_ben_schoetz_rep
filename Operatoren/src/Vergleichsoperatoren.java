/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 13.12.2021
 * @author Ben Sch�tz
 */

public class Vergleichsoperatoren {
  
  public static void main(String[] args) {
    
    int c = 10, d = 5; 															//Deklarartion und Intialisierung von c, d, e und buchstabe
    char buchstabe = 'f'; 
    boolean e = true;
    
    System.out.println(c < 9 || d >= 11);										//�berpr�fung ob c kleiner 9 oder d gr��er/= 11 ist
    System.out.println(c > 9 || (d > 7 && buchstabe == 'f'));					//�berpr�fung ob c kleiner 9 oder d gr��er 7 und ob buchstabe = f
    System.out.println((c < 5 || d < 11) && (buchstabe != 'a' || c == 4));		//�berpr�fung ob c kleiner 5 oder d kleiner 11 und ob buchstabe ungleich a oder c gleich 4 ist  
    System.out.println(!(c == 10) && d <= 11);									//�berpr�fung ob c nicht gleich c und d kleiner gleich 11 ist
    System.out.println(!(c == 10));												//�berpr�fung ob c nicht gleich 10 ist
    System.out.println(e);														//Ausgabe von e 
    System.out.println(!(c != 10) && ((!(buchstabe != 'f') && c < 11) && !e));	//�berpr�fung ob c gleich 10 und buchstabe gleich f und c kleiner als 11 und e nicht wahr ist
        
    
    int a = 2, b = 3;															//Deklaraion und intiallisierung von a, b und name
    char name = 'Q';
    
    System.out.print("System.out.println(a < b);\t\t");							//Ausgabe + hinzuf�gen von abst�nden durch \t\t 
    System.out.println(a < b);													//Berechnung ob a kleiner als b ist
    System.out.print("System.out.println(2*a <= b);\t\t");						//Ausgabe 
    System.out.println(2*a <= b);												//�berpr�fung ob 2 mal a kleiner oder gleich b ist
    System.out.print("System.out.println(name != 'Q');\t");	
    System.out.println(name != 'Q');											//�berpr�fung ob name nicht Q ist
    System.out.print("System.out.println(2*a == a + b -1);\t");
    System.out.println(2*a == a + b -1);										//�berpr�fung ob 2 mal a genauso gro� ist wie a + b - 1
    System.out.print("System.out.println(name == 81);\t\t");
    System.out.println(name == 81);												//�berpr�fung ob name 81 ist
    System.out.print("System.out.println(b-a > 1);\t\t");
    System.out.println(b-a > 1);												//�berpr�fung ob b minus a gr��er als 1 ist
    System.out.print("System.out.println(a != b-1);\t\t");
    System.out.println(a != b-1);												//�berpr�fung ob a nicht so gro� ist wie b-1
  
  
    int x = 2, y = 5, z = 1; 													//Deklaration und Initialisierung von x, y und z
    
    System.out.println (x++);													//Inkrementiert Operator mit 1; die Auswertung erfolgt vor der Inkrementierung
    System.out.println (++x);													//Inkrementiert Operator mit 1; die Auswertung erfolgt nach der Inkrementierung
    x = 1;																		//Initialisierung von x = 1
    x += y;																		//berechnung von x + y
    System.out.println (x);														//Ausgabe von x 
    System.out.println (-y - +y); 												//ausgabe von (-y(-5) minus y(+5) = -10)
    System.out.println (y/x);													//Berechnung y geteilt durch x 
    x = 2;																		//Initialisierung von x = 2
    System.out.println (y%x);													//Modulo (Rest der ganzzahligen Division)
    //System.out.println (y%--z);
    System.out.println(y + " " + x);   											//zur Orientierung
    System.out.println (y+++x++);												//erst nach Ausgabe wird +1 gerechnet
    System.out.println(y + " " + x);   											//zur Orientierung
    System.out.println (++y + ++x);  										    //als Beispiel
    
  } // end of main

} // end of class Vergleichsoperatoren

