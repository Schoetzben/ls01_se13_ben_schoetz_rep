/* Operatoren.java
   Uebung zu Operatoren in Java
*/
public class Operatoren {
      public static void main(String[] args) {
            /* 1. Deklarieren Sie zwei Ganzzahlen.*/
    	  int a, b;

            System.out.println("UEBUNG ZU OPERATOREN IN JAVA");
            System.out.println();

            /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
                  und geben Sie sie auf dem Bildschirm aus. */
            
            a = 75;
            b = 23;
            System.out.println("Ausgabe von a = " + a + " und b = " + b + ".");
            System.out.println();

            /* 3. Addieren Sie die Ganzzahlen
                  und geben Sie das Ergebnis auf dem Bildschirm aus. */
            
            a += b;
            System.out.println("Das Ergebnis von a + b = " + a + ".");
            System.out.println();

            /* 4. Wenden Sie *alle anderen* arithmetischen Operatoren auf die
                  Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
                  Bildschirm aus. */
            
            System.out.println("A und B addiert mit 1 nach Ausgabe " + a++ + " & " + b++ + ".");
            System.out.println("A und B nochmal addiert mit 1, aber vor der Ausgabe " + ++a + " & " + ++b);
            System.out.println("A und B subtrahiert mit 1 nach Ausgabe " + a-- + " & " + b--);
            System.out.println("A und B nochmal subtrahiert mit 1, aber vor der Ausgabe " + --a + " & " + --b);
            System.out.println();

            /* 5. Ueberpruefen Sie, ob die beiden Ganzzahlen gleich sind
                  und geben Sie das Ergebnis auf dem Bildschirm aus. */
            
            System.out.println(a == b);
            System.out.println();
            
            /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
                  und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
            
            System.out.println(a != b);				//ob a ungleich b ist
            System.out.println(a < b);				//ob a kleiner als b ist
            System.out.println(a > b);				//ob a gr��er als b ist

            /* 7. Ueberpruefen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
                  und geben Sie das Ergebnis auf dem Bildschirm aus. 
                  
                  Tipp: Auch das geht nur mit Operatoren!
            */
            
            System.out.println((a >= 0 && a <=50) && (b >= 0 && b <= 50));

      } //Ende von main
} // Ende von Operatoren